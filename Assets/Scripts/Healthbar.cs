﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Healthbar : MonoBehaviour
{
    // Start is called before the first frame update

    public PlayerBehaviour player;
    public Image health;

    float hp, maxHp = 100f;


    void Start()
    {

        hp = maxHp;
        
    }

    // Update is called once per frame
    public void TakeDamage(float amount)
    {

        hp = Mathf.Clamp(hp - amount, 0f, maxHp);
        health.transform.localScale = new Vector2(hp / maxHp, 1);

        /*if(hp <= 0f)
        {

            player.DestroyShip();

        }else if(hp > 0f)
        {



        }*/


    }
}
