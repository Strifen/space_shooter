﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInput : MonoBehaviour
{
    private Vector2 axis;

    public PlayerBehaviour player;
    public Background bg;


    // Update is called once per frame
    void Update()
    {
        axis.x = Input.GetAxis("Horizontal");
        axis.y = Input.GetAxis("Vertical");

        if (Input.GetKeyDown(KeyCode.Z))
        {
            player.NormalShoot();
        }

        if (Input.GetKeyDown(KeyCode.X))
        {
            player.RapidShoot();
        }

        if (Input.GetKeyDown(KeyCode.C))
        {
            player.TipleShoot();
        }

        if (Input.GetKeyDown(KeyCode.V))
        {
            player.BigShoot();
        }

        player.ActualizaDatosInput(axis);
        bg.SetVelocity(axis.x);
    }

}