﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserWeapon : Weapon
{

    public GameObject NormalBullet;
    public GameObject RapidBullet;
    public GameObject TipleBullet;
    public GameObject BigBullet;

    public float cadencia;

    public AudioSource audioSource;

    public override float GetCadencia()
    {
        return cadencia;
    }

    public override void NormalShoot()
    {
        Instantiate(NormalBullet, this.transform.position, Quaternion.identity, null);

        //Play audio
        audioSource.Play();
    }
    

    public override void RapidShoot()
    {
        Instantiate(RapidBullet, this.transform.position, Quaternion.identity, null);

        //Play audio
        //audioSource.Play();
    }

    public override void TipleShoot()
    {
        Instantiate(TipleBullet, this.transform.position, Quaternion.identity, null);
        GameObject go = Instantiate(TipleBullet, this.transform.position, Quaternion.identity, null);
        go.transform.Translate(0, 1, 0);
        GameObject go2 = Instantiate(TipleBullet, this.transform.position, Quaternion.identity, null);
        go2.transform.Translate(0, 0, 0);
        GameObject go3 = Instantiate(TipleBullet, this.transform.position, Quaternion.identity, null);
        go3.transform.Translate(0, -1, 0);

        //Play audio
        //audioSource.Play();
    }

    public override void BigShoot()
    {
        Instantiate(BigBullet, this.transform.position, Quaternion.identity, null);

        //Play audio
        //audioSource.Play();
    }



}
